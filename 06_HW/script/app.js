function renderCurrency(dataForRender) {
  let htmlString = dataForRender.map(function(currency, index) {
    return `<tr>
      <td>${index + 1}</td>
      <td>${currency.r030}</td>
      <td>${currency.txt}</td>
      <td>${(currency.rate).toFixed(2)}</td>
      <td>${currency.cc}</td>
      <td>${currency.exchangedate}</td>
    </tr>`;
  }).join('');

  document.getElementById("countries-tbody").innerHTML = htmlString;
}

fetch('https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json').then(function(data) {
  return data.json();
}).then(function(jsonData){
  renderCurrency(jsonData);
});