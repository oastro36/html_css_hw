function createStorage() {
  let currenciesBackup = [];

  return {
    getCurrencies: function() {
      return currenciesBackup;
    },
    setCurrencies: function(newCurrencies) {
      if(!newCurrencies || !newCurrencies.length) {return;}
      currenciesBackup = newCurrencies;
    }
  }
}

const storage = createStorage();

function renderCurrencies(currencies) {
  let htmlStr = currencies.reduce(function(acc, currency) {
    return acc + `<tr>
      <td>${currency.txt}</td>
      <td>${currency.cc}</td>
      <td>${currency.rate.toFixed(2)}</td>
 </tr>`;
  }, '');

  document.querySelector('table tbody').innerHTML = htmlStr;
}

document.getElementById('select-date').onchange = function(e) {
  let dateValue = (e.currentTarget.value).replaceAll("-", "");
  let dateString = `https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?date=${dateValue}&json`;

fetch(dateString).then(function(data) {
  return data.json();
}).then(function(data) {
  storage.setCurrencies(data);
  renderCurrencies(data);
})

}

document.getElementById('search').onkeyup = function(e) {
  const currentSearch = e.currentTarget.value.toLowerCase().trim();
  const backup = storage.getCurrencies();
  const filteredCurrencies = backup.filter(function(currency) {
    return currency.txt.toLowerCase().includes(currentSearch) ||
        currency.cc.toLowerCase().includes(currentSearch);
  })
  renderCurrencies(filteredCurrencies);
}